/*
** $Id: loadconf.h 1654 2006-05-29 18:18:02Z andre $
** watchcatd - Watchcat Daemon
** See copyright notice in distro's COPYRIGHT file
*/

#ifndef LOADCONF_H
#define LOADCONF_H

void load_conf(char *);

#endif /* LOADCONF_H */
