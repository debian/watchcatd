# $Id: watchcatd.spec 2831 2009-02-13 21:02:56Z andre.dig $

%define name    watchcatd
%define version 1.2.1
%define release 1mdv

Summary:        Software watchdog, but less drastic than the usual solutions
Name:           %name
Version:        %version
Release:        %release
License:        GPL
Group:          System/Servers
URL:            http://oss.digirati.com.br/watchcatd/

# Please add comment with the right url/downloadpage.
Source0:        %name-%version.tar.bz2

BuildRoot:      %_tmppath/%name-buildroot
PreReq:         rpm-helper

%description
watchcatd is a software service that monitors processes. A bug or
malicious attacks to machine can lock up a process, leading to a
deadlock or an unexpected condition. If a monitored process locks
up, it is killed. 

On an alternate setup, watchcatd can be configured to work as a
timer.

%prep
%setup -q

%build
export CFLAGS="%{optflags}"
export PREFIX="/usr"
%make

%install
%__rm -rf %buildroot
install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_libdir}/watchcatd
install -d %{buildroot}%{_sysconfdir}/init.d
install -d %{buildroot}%{_mandir}/man{5,8}
install -d %{buildroot}%{_docdir}/watchcatd
install -m0750 catmaster %{buildroot}%{_libdir}/watchcatd
install -m0750 catslave %{buildroot}%{_libdir}/watchcatd
install -m0700 wcatstat %{buildroot}%{_sbindir}/wcatstat
install -m0700 watchcatd.init %{buildroot}%{_sysconfdir}/init.d/watchcatd
install -m0600 watchcatd.prod.conf %{buildroot}%{_sysconfdir}/watchcatd.conf
install -m0644 watchcatd.8 %{buildroot}%{_mandir}/man8/
install -m0644 watchcatd.conf.5 %{buildroot}%{_mandir}/man5/

%pre
%_pre_useradd watchcat /var/lib/watchcat /sbin/nologin
install -m0700 -d /var/lib/watchcat
chown root:root /var/lib/watchcat

%post
%_post_service watchcatd

%preun
%_preun_service watchcatd

%postun
if [ "$1" = "0" ]; then
  %_postun_userdel watchcat
  %__rm -rf /var/lib/watchcat
fi

%clean
%__rm -rf %buildroot

%files
%defattr(-,root,root)
%{_sbindir}/wcatstat
%{_libdir}/watchcatd/catmaster
%{_libdir}/watchcatd/catslave
%config(noreplace) %{_sysconfdir}/init.d/watchcatd
%config(noreplace) %{_sysconfdir}/watchcatd.conf
%{_mandir}/man8/%name.8*
%{_mandir}/man5/%name.conf.5*
%doc COPYRIGHT ChangeLog TODO

%changelog
* Fri Feb  13 2009 Andre Nathan <andre@digirati.com.br> 1.2.1-1mdv
- Version 1.2.1.

* Wed Aug  27 2008 Andre Nathan <andre@digirati.com.br> 1.2-1mdk
- Version 1.2.

* Mon Apr  30 2004 Andre Nathan <andre@digirati.com.br> 1.1-3mdk
- %_preun_service shall be executed during upgrades.

* Mon Apr  19 2004 Andre Nathan <andre@digirati.com.br> 1.1-2mdk
- Fix some bugs.

* Fri Apr  09 2004 Andre Nathan <andre@digirati.com.br> 1.1-1mdk
- Watchcatd 1.1

* Thu Feb  05 2004 Andre Nathan <andre@digirati.com.br> 1.0-2
- Export CFLAGS before building;
- Fix typo in changelog.

* Mon Feb  02 2004 Michel Machado <michel@digirati.com.br> 1.0-1
- Adjust version and watchcat user;
- Restrict mode access for files;
- Add COPYRIGHT.

* Wed Jan  21 2004 Andre Nathan <andre@digirati.com.br> 0.0-2
- Add watchcatd user and macros for turning the service on.

* Wed Jan  14 2004 Andre Nathan <andre@digirati.com.br> 0.0-1
- First version.
