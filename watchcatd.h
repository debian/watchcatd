/*
** $Id: watchcatd.h 1654 2006-05-29 18:18:02Z andre $
** watchcatd - Watchcat Daemon
** See copyright notice in distro's COPYRIGHT file
*/

#ifndef WATCHCATD_H
#define WATCHCATD_H

typedef struct {
    /* Common settings. */
    int realtime_mode;
    int priority;
    char user[256];

    /* Master settings only. */
    struct {
        int debug_level;
        char pid_file[256];
    } master;
    
    /* Slave settings only. */
    struct {
        int debug_level;
        char device[256];
        int max_conections;
        int max_conections_per_pid;
    } slave;

} WATCHCAT_GLOBAL_CONF;

extern WATCHCAT_GLOBAL_CONF GLOBAL_CONF;

#endif /* WATCHCATD_H */
