/*
** $Id: utils.h 2699 2008-08-27 15:02:49Z andre.dig $
** watchcatd - Watchcat Daemon
** See copyright notice in distro's COPYRIGHT file
*/

#ifndef UTILS_H
#define UTILS_H

#include <assert.h>

#ifndef __ASSERT_FUNCTION
#ifdef __PRETTY_FUNCTION__
#define __ASSERT_FUNCTION __PRETTY_FUNCTION__
#else
#define __ASSERT_FUNCTION __func__
#endif
#endif

#define myassert(exp) \
    ((void) ((exp) ? 0 : _myassert( \
    #exp, __FILE__, __LINE__, __ASSERT_FUNCTION)))

#define myassert_sys(exp) \
    ((void) ((exp) ? 0 : _myassert_sys( \
    #exp, __FILE__, __LINE__, __ASSERT_FUNCTION)))

int _myassert(char *exp, char *filename, int line, const char *func);
int _myassert_sys(char *exp, char *filename, int line, const char *func);
extern void mysyslog(int priority, const char *format, ...);

int is_fd_open(int fd);

void protect_process_group(int nodaemon, int priority);
void protect_daemon(int realtime);
void drop_privileges(char *user);

#endif /* UTILS_H */
