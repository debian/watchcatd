PKGNAME= watchcatd
PKGVERSION= 1.2.1

PREFIX ?= /usr/local

CFLAGS += -O2 -Wall -pedantic -L$(PREFIX)/lib -I$(PREFIX)/include -DWCAT_PREFIX=\"$(PREFIX)\"
#CFLAGS += -O2 -Wall -pedantic -L$(PREFIX)/lib -I$(PREFIX)/include -DWCAT_PREFIX=\"$(PREFIX)\" -ggdb

all: catmaster catslave

catmaster: utils.o master.o loadconf.o watchcatd.o
	$(CC) utils.o master.o loadconf.o watchcatd.o -levent -o catmaster $(CFLAGS)

catslave: utils.o slave.o watchcatd.o
	$(CC) utils.o slave.o watchcatd.o -levent -o catslave $(CFLAGS)

watchcatd.o: watchcatd.h watchcatd.c
	$(CC) -c watchcatd.c $(CFLAGS)

master.o: master.h utils.h watchcatd.h loadconf.h master.c
	$(CC) -c master.c $(CFLAGS)

slave.o: slave.h master.h utils.h watchcatd.h slave.c
	$(CC) -c slave.c $(CFLAGS)

utils.o: utils.h utils.c
	$(CC) -c utils.c $(CFLAGS)

loadconf.o: loadconf.h loadconf.c
	$(CC) -c loadconf.c $(CFLAGS)

install:
	strip catmaster catslave
	install -d $(PREFIX)/lib/watchcatd
	install -d $(PREFIX)/man/man5
	install -d $(PREFIX)/man/man8
	install -m0755 catmaster $(PREFIX)/lib/watchcatd
	install -m0755 catslave $(PREFIX)/lib/watchcatd
	install -m0600 watchcatd.prod.conf /etc/watchcatd.conf
	install -m0644 watchcatd.8 $(PREFIX)/man/man8/
	install -m0644 watchcatd.conf.5 $(PREFIX)/man/man5/

clean:
	rm -f *.o *.c~ *.h~ *.conf~ *.[0-9]~ catmaster catslave
	rm -f COPYRIGHT~ TODO~ Makefile~ *.init~ $(PKGNAME)-$(PKGVERSION).tar.bz2
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  rm -rf ../$(PKGNAME)-$(PKGVERSION); \
	fi

rpm: clean
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  cp -a . ../$(PKGNAME)-$(PKGVERSION); \
	fi
	tar -jcC .. --exclude .svn --exclude libwcat --exclude tester -f $(PKGNAME)-$(PKGVERSION).tar.bz2 $(PKGNAME)-$(PKGVERSION)
	rpm -ta $(PKGNAME)-$(PKGVERSION).tar.bz2
	rm -rf $(PKGNAME)-$(PKGVERSION).tar.bz2
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  rm -rf ../$(PKGNAME)-$(PKGVERSION); \
	fi
