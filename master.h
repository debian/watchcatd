/*
** $Id: master.h 1654 2006-05-29 18:18:02Z andre $
** watchcatd - Watchcat Daemon
** See copyright notice in distro's COPYRIGHT file
*/

#ifndef MASTER_H
#define MASTER_H

#define FROM_SLAVE_TO_MASTER_FD 3
#define SOCKET_LISTENER_FD      4

#define MSG_KILL 0
#define MSG_LOG  1

struct kill_msg {
    int fd;
    pid_t pid;
    int signal;
};

struct log_msg {
    int priority;
    int length;
};

#endif /* MASTER_H */
